package simplifii.framework.utility;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

/**
 * Created by Dell on 9/25/2016.
 */
public class DomParserUtils {

    public static String parseXmlToNodeName(String string, String nodeName) throws TransformerException, ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new ByteArrayInputStream(string.getBytes("utf-8"))));
        Transformer trans = TransformerFactory.newInstance().newTransformer(
                new StreamSource(new StringReader("<?xml version='1.0'?>"
                        + "<stylesheet xmlns='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
                        + "<template match='*' priority='1'>"
                        + "<element name='{local-name()}'><apply-templates select='@*|node()'/></element>"
                        + "</template>"
                        + "<template match='@*' priority='0'>"
                        + "<attribute name='{local-name()}'><value-of select='.'/></attribute>"
                        + "</template>"
                        + "<template match='node()' priority='-1'>"
                        + "<copy><apply-templates select='@*|node()'/></copy>"
                        + "</template>"
                        + "</stylesheet>")));
        DOMResult result = new DOMResult();
        trans.transform(new DOMSource(document), result);
        document = (Document) result.getNode();
        NodeList dataroot = document.getElementsByTagName(nodeName);
        String response = null;
        if(dataroot!=null){
            Node answersNode = dataroot.item(0);
            response = answersNode.getFirstChild().getTextContent();
        }
        return response;
    }
}
