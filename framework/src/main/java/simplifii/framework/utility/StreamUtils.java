package simplifii.framework.utility;

import android.content.Context;
import android.text.Html;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;

/**
 * Created by Dell on 9/26/2016.
 */
public class StreamUtils {

    public static InputStream getInputStreamFromAssetFile(Context ctx, String fileName){
        try {
            InputStream is = ctx.getAssets().open(fileName);
            return is;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String convertAssetFileToString(Context ctx, String fileName) throws UnsupportedEncodingException {
        InputStream inputStream = getInputStreamFromAssetFile(ctx, fileName);
        String response = null;
        if(inputStream!=null){
            response = convertStreamToString(inputStream);
        }
        return response;
    }

    public static String convertObjectToXml(Object object) throws JSONException {
        String json = JsonUtil.getJsonString(object);
        return convertJsonStringToXmlString(json);
    }

    public static String convertJsonStringToXmlString(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        return XML.toString(jsonObject);
    }

    public static String getTokenForOtp(String requestNo, String otp){
        StringBuilder sb = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        String year = String.valueOf(cal.get(Calendar.YEAR));
        sb.append(year);
        int monthNo = cal.get(Calendar.MONTH);
        if(monthNo<10){
            sb.append("0");
        }
        sb.append(monthNo);
        int day = cal.get(Calendar.DATE);
        if(day<10){
            sb.append("0");
        }
        sb.append(day);
        sb.append(requestNo).append(otp);
        return sb.toString();
    }
}
