package simplifii.framework.utility;

import java.util.Collection;

/**
 * Created by Dell on 9/26/2016.
 */
public class CollectionsUtils {

    public static boolean isEmpty(Collection collection){
        return (collection==null||collection.isEmpty());
    }

    public static boolean isNotEmpty(Collection collection){
        return !isEmpty(collection);
    }
}
