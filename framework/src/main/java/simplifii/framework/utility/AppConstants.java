package simplifii.framework.utility;

import java.util.HashMap;
import java.util.LinkedHashMap;

public interface AppConstants {

    public static final String DEF_REGULAR_FONT = "Roboto-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    LinkedHashMap<Integer,String> storeCategory=new LinkedHashMap<Integer, String>();
    String REGISTRATION_COMPLETE = "registrationComplete";
    String APPLICATION_JSON = "application/json";
    String ENCRYPT_PASSWORD_URL = "https://uat.myfatoora.com/Handlers/UserProfile.ashx?type=encryptpassword";
    String CONTENT_LENGTH = "Content-Length";

    interface REQUEST_CODES{

        int GOOGLE_SIGHN_IN = 10;
        int REGISTER = 11;
    }

    public static interface VALIDATIONS {
        String EMPTY = "empty";
        String EMAIL = "email";
        String MOBILE = "mobile";
    }

    public static interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
    }


    public static interface ERROR_CODES {

        public static final int UNKNOWN_ERROR = 0;
        public static final int NO_INTERNET_ERROR = 1;
        public static final int NETWORK_SLOW_ERROR = 2;
        public static final int URL_INVALID = 3;
        public static final int DEVELOPMENT_ERROR = 4;

    }

    public static interface PAGE_URL {
        String LOGIN = "http://byte-matrix.com/demo/entertainer/api/login";
        String REGISTER = "http://byte-matrix.com/demo/entertainer/api/register";
        String FBREGISTER = "http://byte-matrix.com/demo/entertainer/api/register";
        String FBLOGIN = "http://byte-matrix.com/demo/entertainer/api/login";
        String OFFERLOCKED = "http://byte-matrix.com/demo/entertainer/api/merchants/locked";
        String ALLOFFERS = "http://byte-matrix.com/demo/entertainer/api/merchants";
        String MONTHOFFERS = "http://byte-matrix.com/demo/entertainer/api/merchants/monthly";
        String NEWOFFERS = "http://byte-matrix.com/demo/entertainer/api/merchants/new";
    }

    public static interface PREF_KEYS {

        String KEY_LOGIN = "IsUserLoggedIn";
        String KEY_USERNAME = "username";
        String KEY_PASSWORD = "password";
        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";
        String USER_TOKEN = "user_token";
        String IS_LOGIN = "is_login";
        String IS_FIRST = "is_first";
    }

    public static interface BUNDLE_KEYS {
        public static final String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
    }


    public static interface VIEW_TYPE {
        int CARD_MY_TEAM = 0;
    }
    public static interface MEDIA_TYPES{
        String IMAGE = "img";
        String AUDIO = "audio";
        String VIDEO = "video";
    }

    public interface TASKCODES {
        int LOGIN = 20;
        int VERIFY_REGISTER = 21;
        int FBREGISTER = 22;
        int FBLOGIN = 23;
        int OFFER = 31;
        int ENCRYPT_PASSWORD = 32;
        int GET_SECURITY_QUESTIONS = 33;
        int GENERATE_OTP = 34;
        int VALIDATE_OTP = 35;
        int REGISTER = 36;
        int VERIFY_MOBILE = 37;
        int VALIDATE_CUSTOMER = 38;
    }
}
