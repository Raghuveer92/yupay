package simplifii.framework.utility;

import android.content.Context;
import android.text.TextUtils;

import java.io.UnsupportedEncodingException;

import simplifii.framework.soap.requests.GetSecurityQuestions;

/**
 * Created by Dell on 9/26/2016.
 */
public interface SoapApiUtils {

    public static final String SOAP_BASE_API_FILE_NAME = "soap.txt";
    public static final String CDATA_XML_STRING = "<![CDATA[]]>";
    public static final String SOAP_BASE_API_BODY_PLACEHOLDER = "PLACEHOLDER2";
    public static final String SOAP_BASE_API_METHOD_PLACEHOLDER = "PLACEHOLDER1";
    public static final String API_URL = "https://uat.myfatoora.com/Gateway/KioskStudioService.svc/";
    public static final String API_HOST = "uat.myfatoora.com";
    public static final String API_ACTION = "http://tempuri.org/IKioskStudioService/";
    public static final String API_CONTENT_TYPE = "application/soap+xml;charset=UTF-8";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_MOBILE = "mobile";
    public static final String ACTION_GET_SECURITY_QUESTIONS = "GetSecurityQuestions";
    public static final String ACTION_GENERATE_OTP = "GenerateOTP";
    public static final String NODE_NAME_GET_SECURITY_QUESTIONS = "GetSecurityQuestionsResult";
    public static final String NODE_NAME_GENERATE_OTP = "GenerateOTPResult";
    public static final String ACTION_VALIDATE_OTP = "ValidateOTP";
    public static final String NODE_NAME_VALIDATE_OTP = "ValidateOTPResult";
    public static final String KEY_OPT_REQUEST_NO = "otpRequestNo";
    String KEY_OTP = "keyOtp";
    String ACTION_REGISTER_USER = "RegisterCustomer";
    String NODE_NAME_REGISTER_CUSTOMER = "RegisterCustomerResult";
    String KEY_ENCRYPTED_PASSWORD = "encryptedPassword";
    String KEY_HASHKEY = "hashkey";
    String KEY_PASSWORD = "password";
    String ACTION_NAME_VALIDATE_CUSTOMER = "ValidateCustomer";
    String NODE_NAME_VALIDATE_CUSTOMER = "ValidateCustomerResult";

}
