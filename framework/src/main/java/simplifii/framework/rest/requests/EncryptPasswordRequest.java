package simplifii.framework.rest.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 9/27/2016.
 */
public class EncryptPasswordRequest {

    @Expose
    @SerializedName(value = "Tobeencryptedstring")
    private String toBeEncrypted;

    @Expose
    @SerializedName(value = "Password")
    private String password;

    public EncryptPasswordRequest(){}

    public EncryptPasswordRequest(String toBeEncrypted, String password) {
        this.toBeEncrypted = toBeEncrypted;
        this.password = password;
    }

    public String getToBeEncrypted() {
        return toBeEncrypted;
    }

    public void setToBeEncrypted(String toBeEncrypted) {
        this.toBeEncrypted = toBeEncrypted;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "EncryptPasswordRequest{" +
                "toBeEncrypted='" + toBeEncrypted + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
