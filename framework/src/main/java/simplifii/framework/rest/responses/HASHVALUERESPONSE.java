package simplifii.framework.rest.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HashValueResponse
{
    @Expose
    @SerializedName(value = "RESPONSECODE")
    private String responseCode;

    @Expose
    @SerializedName(value = "AESENCRYPTEDVALUES")
    private String aesEncrytpedValues;

    @Expose
    @SerializedName(value = "RESPONSEMESSAGE")
    private String responseMessage;

    @Expose
    @SerializedName(value = "SALTVALUES")
    private String saltValues;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getAesEncrytpedValues() {
        return aesEncrytpedValues;
    }

    public void setAesEncrytpedValues(String aesEncrytpedValues) {
        this.aesEncrytpedValues = aesEncrytpedValues;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getSaltValues() {
        return saltValues;
    }

    public void setSaltValues(String saltValues) {
        this.saltValues = saltValues;
    }

    @Override
    public String toString() {
        return "HashValueResponse{" +
                "responseCode='" + responseCode + '\'' +
                ", aesEncrytpedValues='" + aesEncrytpedValues + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", saltValues='" + saltValues + '\'' +
                '}';
    }
}