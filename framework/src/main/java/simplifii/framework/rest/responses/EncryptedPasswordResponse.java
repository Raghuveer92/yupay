package simplifii.framework.rest.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EncryptedPasswordResponse
{
    @Expose
    @SerializedName(value = "HASHVALUERESPONSE")
    private HashValueResponse hashValueResponse;

    public HashValueResponse getHashValueResponse()
    {
        return hashValueResponse;
    }

    public void setHashValueResponse(HashValueResponse hashValueResponse)
    {
        this.hashValueResponse = hashValueResponse;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [HASHVALUERESPONSE = "+ hashValueResponse +"]";
    }
}