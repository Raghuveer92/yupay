package simplifii.framework.soap.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidateOTPResponse {

    @Expose
    @SerializedName(value = "RESPONSECODE")
    private String responseCode;

    @Expose
    @SerializedName(value = "RESPONSEMESSAGE")
    private String responseMessage;

    @Expose
    @SerializedName(value = "ISVALID")
    private String isValid;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getIsValid() {
        return isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    @Override
    public String toString() {
        return "VALIDATEOTPRESPONSE{" +
                "responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", isValid='" + isValid + '\'' +
                '}';
    }
}