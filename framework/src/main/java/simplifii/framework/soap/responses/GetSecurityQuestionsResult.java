package simplifii.framework.soap.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import pt.joaocruz04.lib.annotations.JSoapResField;

/**
 * Created by Dell on 9/25/2016.
 */
public class GetSecurityQuestionsResult {

    @Expose()
    @SerializedName(value = "SECURITYQUESTIONDETAILSRESPONSE")
    private SecurityQuestionDetailResponse securityQuestionDetailResponse;

    public SecurityQuestionDetailResponse getSecurityQuestionDetailResponse() {
        return securityQuestionDetailResponse;
    }

    public void setSecurityQuestionDetailResponse(SecurityQuestionDetailResponse securityQuestionDetailResponse) {
        this.securityQuestionDetailResponse = securityQuestionDetailResponse;
    }

    @Override
    public String toString() {
        return "GetSecurityQuestionsResult{" +
                "securityQuestionDetailResponse=" + securityQuestionDetailResponse +
                '}';
    }
}
