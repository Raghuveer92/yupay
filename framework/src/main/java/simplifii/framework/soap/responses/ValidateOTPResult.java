package simplifii.framework.soap.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.soap.requests.ValidateOTPRequest;

/**
 * Created by Dell on 9/26/2016.
 */
public class ValidateOTPResult {

    @Expose
    @SerializedName(value = "VALIDATEOTPRESPONSE")
    private ValidateOTPResponse validateOTPResponse;

    public ValidateOTPResponse getValidateOTPResponse() {
        return validateOTPResponse;
    }

    public void setValidateOTPResponse(ValidateOTPResponse validateOTPResponse) {
        this.validateOTPResponse = validateOTPResponse;
    }

    @Override
    public String toString() {
        return "ValidateOTP{" +
                "validateOTPResponse=" + validateOTPResponse +
                '}';
    }
}
