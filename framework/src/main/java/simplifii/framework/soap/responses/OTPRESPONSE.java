package simplifii.framework.soap.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTPResponse
{
    @Expose
    @SerializedName(value = "OTPREQUESTNO")
    private String otpRequestNo;

    @Expose
    @SerializedName(value = "CUSTMOBILENO")
    private String custMobileNo;

    @Expose
    @SerializedName(value = "CUSTOMERID")
    private String custId;

    public String getOtpRequestNo() {
        return otpRequestNo;
    }

    public void setOtpRequestNo(String otpRequestNo) {
        this.otpRequestNo = otpRequestNo;
    }

    public String getCustMobileNo() {
        return custMobileNo;
    }

    public void setCustMobileNo(String custMobileNo) {
        this.custMobileNo = custMobileNo;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    @Override
    public String toString() {
        return "OTPResponse{" +
                "otpRequestNo='" + otpRequestNo + '\'' +
                ", custMobileNo='" + custMobileNo + '\'' +
                ", custId='" + custId + '\'' +
                '}';
    }
}