package simplifii.framework.soap.responses;

public class RegisterCustomerResponse
{
    private String RESPONSECODE;

    private String RESPONSEMESSAGE;

    private String CUSTOMERID;

    public String getRESPONSECODE ()
    {
        return RESPONSECODE;
    }

    public void setRESPONSECODE (String RESPONSECODE)
    {
        this.RESPONSECODE = RESPONSECODE;
    }

    public String getRESPONSEMESSAGE ()
    {
        return RESPONSEMESSAGE;
    }

    public void setRESPONSEMESSAGE (String RESPONSEMESSAGE)
    {
        this.RESPONSEMESSAGE = RESPONSEMESSAGE;
    }

    public String getCUSTOMERID ()
    {
        return CUSTOMERID;
    }

    public void setCUSTOMERID (String CUSTOMERID)
    {
        this.CUSTOMERID = CUSTOMERID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RESPONSECODE = "+RESPONSECODE+", RESPONSEMESSAGE = "+RESPONSEMESSAGE+", CUSTOMERID = "+CUSTOMERID+"]";
    }
}