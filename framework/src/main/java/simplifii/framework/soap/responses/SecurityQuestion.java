package simplifii.framework.soap.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import pt.joaocruz04.lib.annotations.JSoapResField;

/**
 * Created by Dell on 9/25/2016.
 */
public class SecurityQuestion {

    @Expose()
    @SerializedName(value = "QUESTION")
    private String question;

    @Expose()
    @SerializedName(value = "ID")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "SecurityQuestion{" +
                "question='" + question + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
