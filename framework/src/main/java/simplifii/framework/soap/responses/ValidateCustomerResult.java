package simplifii.framework.soap.responses;

import org.json.JSONObject;

import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

public class ValidateCustomerResult
{
    private ValidateCustomerResponse VALIDATECUSTOMERRESPONSE;
    private static ValidateCustomerResult runningInstance;

    public static ValidateCustomerResult getRunningInstance() {
        if(runningInstance==null){
            String json = Preferences.getData(Preferences.KEY_CUSTOMER_DATA,"");
            runningInstance = (ValidateCustomerResult) JsonUtil.parseJson(json, ValidateCustomerResult.class);
        }
        return runningInstance;
    }

    public ValidateCustomerResponse getVALIDATECUSTOMERRESPONSE ()
    {
        return VALIDATECUSTOMERRESPONSE;
    }

    public void setVALIDATECUSTOMERRESPONSE (ValidateCustomerResponse VALIDATECUSTOMERRESPONSE)
    {
        this.VALIDATECUSTOMERRESPONSE = VALIDATECUSTOMERRESPONSE;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [VALIDATECUSTOMERRESPONSE = "+VALIDATECUSTOMERRESPONSE+"]";
    }

    public static ValidateCustomerResult parseJson(String json){
        runningInstance = (ValidateCustomerResult) JsonUtil.parseJson(json, ValidateCustomerResult.class);
        if(runningInstance!=null){
            if(runningInstance.getVALIDATECUSTOMERRESPONSE()!=null&&"0".equals(runningInstance.getVALIDATECUSTOMERRESPONSE().getRESPONSECODE())){
                Preferences.saveData(Preferences.KEY_CUSTOMER_DATA,json);
            }
        }
        return runningInstance;
    }
}