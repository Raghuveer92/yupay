package simplifii.framework.soap.responses;

public class RegisterCustomerResult
{
    private RegisterCustomerResponse REGISTERCUSTOMERRESPONSE;

    public RegisterCustomerResponse getREGISTERCUSTOMERRESPONSE ()
    {
        return REGISTERCUSTOMERRESPONSE;
    }

    public void setREGISTERCUSTOMERRESPONSE (RegisterCustomerResponse REGISTERCUSTOMERRESPONSE)
    {
        this.REGISTERCUSTOMERRESPONSE = REGISTERCUSTOMERRESPONSE;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [REGISTERCUSTOMERRESPONSE = "+REGISTERCUSTOMERRESPONSE+"]";
    }
}