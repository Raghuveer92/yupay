package simplifii.framework.soap.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import pt.joaocruz04.lib.annotations.JSoapResField;

/**
 * Created by Dell on 9/25/2016.
 */
public class SecurityQuestionDetailResponse {

    @Expose()
    @SerializedName(value = "RESPONSECODE")
    private String responseCode;

    @Expose()
    @SerializedName(value = "RESPONSEMESSAGE")
    private String responseMessage;

    @Expose()
    @SerializedName(value = "SECURITYQUESTION")
    private List<SecurityQuestion> securityQuestionList;

    public List<SecurityQuestion> getSecurityQuestionList() {
        return securityQuestionList;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public void setSecurityQuestionList(List<SecurityQuestion> securityQuestionList) {
        this.securityQuestionList = securityQuestionList;
    }

    @Override
    public String toString() {
        return "SecurityQuestionDetailResponse{" +
                "securityQuestionList=" + securityQuestionList +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                '}';
    }
}
