package simplifii.framework.soap.responses;

public class ValidateCustomerResponse
{
    private String CUSTFULLNAME;

    private String TXNPASSWORDEXPIRESIN;

    private String RESPONSECODE;

    private String SESSIONLOGINID;

    private String RESPONSEMESSAGE;

    private String CUSTEMAILID;

    private String PASSWORDEXPIRESIN;

    private String CUSTOMERID;

    private String CUSTMOBILENO;

    private String PROFILECOMPLETION;

    public String getCUSTFULLNAME ()
    {
        return CUSTFULLNAME;
    }

    public void setCUSTFULLNAME (String CUSTFULLNAME)
    {
        this.CUSTFULLNAME = CUSTFULLNAME;
    }

    public String getTXNPASSWORDEXPIRESIN ()
    {
        return TXNPASSWORDEXPIRESIN;
    }

    public void setTXNPASSWORDEXPIRESIN (String TXNPASSWORDEXPIRESIN)
    {
        this.TXNPASSWORDEXPIRESIN = TXNPASSWORDEXPIRESIN;
    }

    public String getRESPONSECODE ()
    {
        return RESPONSECODE;
    }

    public void setRESPONSECODE (String RESPONSECODE)
    {
        this.RESPONSECODE = RESPONSECODE;
    }

    public String getSESSIONLOGINID ()
    {
        return SESSIONLOGINID;
    }

    public void setSESSIONLOGINID (String SESSIONLOGINID)
    {
        this.SESSIONLOGINID = SESSIONLOGINID;
    }

    public String getRESPONSEMESSAGE ()
    {
        return RESPONSEMESSAGE;
    }

    public void setRESPONSEMESSAGE (String RESPONSEMESSAGE)
    {
        this.RESPONSEMESSAGE = RESPONSEMESSAGE;
    }

    public String getCUSTEMAILID ()
    {
        return CUSTEMAILID;
    }

    public void setCUSTEMAILID (String CUSTEMAILID)
    {
        this.CUSTEMAILID = CUSTEMAILID;
    }

    public String getPASSWORDEXPIRESIN ()
    {
        return PASSWORDEXPIRESIN;
    }

    public void setPASSWORDEXPIRESIN (String PASSWORDEXPIRESIN)
    {
        this.PASSWORDEXPIRESIN = PASSWORDEXPIRESIN;
    }

    public String getCUSTOMERID ()
    {
        return CUSTOMERID;
    }

    public void setCUSTOMERID (String CUSTOMERID)
    {
        this.CUSTOMERID = CUSTOMERID;
    }

    public String getCUSTMOBILENO ()
    {
        return CUSTMOBILENO;
    }

    public void setCUSTMOBILENO (String CUSTMOBILENO)
    {
        this.CUSTMOBILENO = CUSTMOBILENO;
    }

    public String getPROFILECOMPLETION ()
    {
        return PROFILECOMPLETION;
    }

    public void setPROFILECOMPLETION (String PROFILECOMPLETION)
    {
        this.PROFILECOMPLETION = PROFILECOMPLETION;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CUSTFULLNAME = "+CUSTFULLNAME+", TXNPASSWORDEXPIRESIN = "+TXNPASSWORDEXPIRESIN+", RESPONSECODE = "+RESPONSECODE+", SESSIONLOGINID = "+SESSIONLOGINID+", RESPONSEMESSAGE = "+RESPONSEMESSAGE+", CUSTEMAILID = "+CUSTEMAILID+", PASSWORDEXPIRESIN = "+PASSWORDEXPIRESIN+", CUSTOMERID = "+CUSTOMERID+", CUSTMOBILENO = "+CUSTMOBILENO+", PROFILECOMPLETION = "+PROFILECOMPLETION+"]";
    }
}