package simplifii.framework.soap.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenerateOTPResult
{
    @Expose
    @SerializedName(value = "OTPRESPONSE")
    private OTPResponse otpresponse;

    public OTPResponse getOtpresponse() {
        return otpresponse;
    }

    public void setOtpresponse(OTPResponse otpresponse) {
        this.otpresponse = otpresponse;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [OTPRESPONSE = "+otpresponse+"]";
    }
}
