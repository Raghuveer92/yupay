package simplifii.framework.soap.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 9/26/2016.
 */
public class ValidateOtp {

    @Expose
    @SerializedName(value = "VALIDATEOTPREQUEST")
    private ValidateOTPRequest validateOTPRequest;

    public ValidateOTPRequest getValidateOTPRequest() {
        return validateOTPRequest;
    }

    public void setValidateOTPRequest(ValidateOTPRequest validateOTPRequest) {
        this.validateOTPRequest = validateOTPRequest;
    }

    @Override
    public String toString() {
        return "ValidateOtp{" +
                "validateOTPRequest=" + validateOTPRequest +
                '}';
    }
}
