package simplifii.framework.soap.requests;

/**
 * Created by Dell on 9/27/2016.
 */
public class RegisterCustomer {
    private RegisterCustomerRequest REGISTERCUSTOMERREQUEST;

    public RegisterCustomerRequest getREGISTERCUSTOMERREQUEST ()
    {
        return REGISTERCUSTOMERREQUEST;
    }

    public void setREGISTERCUSTOMERREQUEST (RegisterCustomerRequest REGISTERCUSTOMERREQUEST)
    {
        this.REGISTERCUSTOMERREQUEST = REGISTERCUSTOMERREQUEST;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [REGISTERCUSTOMERREQUEST = "+REGISTERCUSTOMERREQUEST+"]";
    }
}
