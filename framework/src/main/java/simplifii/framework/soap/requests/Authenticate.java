package simplifii.framework.soap.requests;

public class Authenticate
{
    private AuthenticationRequest AUTHENTICATIONREQUEST;

    public AuthenticationRequest getAUTHENTICATIONREQUEST ()
    {
        return AUTHENTICATIONREQUEST;
    }

    public void setAUTHENTICATIONREQUEST (AuthenticationRequest AUTHENTICATIONREQUEST)
    {
        this.AUTHENTICATIONREQUEST = AUTHENTICATIONREQUEST;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [AUTHENTICATIONREQUEST = "+AUTHENTICATIONREQUEST+"]";
    }
}