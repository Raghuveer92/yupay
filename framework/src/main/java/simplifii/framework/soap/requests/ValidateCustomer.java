package simplifii.framework.soap.requests;

public class ValidateCustomer
{
    private ValidateCustomerRequest VALIDATECUSTOMERREQUEST;

    public ValidateCustomerRequest getVALIDATECUSTOMERREQUEST ()
    {
        return VALIDATECUSTOMERREQUEST;
    }

    public void setVALIDATECUSTOMERREQUEST (ValidateCustomerRequest VALIDATECUSTOMERREQUEST)
    {
        this.VALIDATECUSTOMERREQUEST = VALIDATECUSTOMERREQUEST;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [VALIDATECUSTOMERREQUEST = "+VALIDATECUSTOMERREQUEST+"]";
    }
}