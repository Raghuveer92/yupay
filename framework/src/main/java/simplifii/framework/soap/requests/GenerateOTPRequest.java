package simplifii.framework.soap.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 9/26/2016.
 */
public class GenerateOTPRequest {

    @Expose
    @SerializedName(value = "CUSTEMAIL")
    private String custEmail;

    @Expose
    @SerializedName(value = "TERMINALID")
    private String terminalId="1";

    @Expose
    @SerializedName(value = "TRANSACTIONTYPE")
    private String transactionType="RP";

    @Expose
    @SerializedName(value = "MERCHANTID")
    private String merchantId="57";

    @Expose
    @SerializedName(value = "CHANNELID")
    private String channelId="2";

    @Expose
    @SerializedName(value = "CUSTMOBILENO")
    private String custMobile;

    public String getChannelId() {
        return channelId;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    @Override
    public String toString() {
        return "GenerateOTPRequest{" +
                "custEmail='" + custEmail + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", channelId='" + channelId + '\'' +
                ", custMobile='" + custMobile + '\'' +
                '}';
    }
}
