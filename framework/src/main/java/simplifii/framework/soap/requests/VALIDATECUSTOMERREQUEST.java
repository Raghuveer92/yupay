package simplifii.framework.soap.requests;

public class ValidateCustomerRequest
{
    private String CUSTPROFILEPASSWORD;

    private String CUSTUSERNAME;

    private String MERCHANTID="57";

    private String CHANNELID="2";

    public String getCUSTPROFILEPASSWORD ()
    {
        return CUSTPROFILEPASSWORD;
    }

    public void setCUSTPROFILEPASSWORD (String CUSTPROFILEPASSWORD)
    {
        this.CUSTPROFILEPASSWORD = CUSTPROFILEPASSWORD;
    }

    public String getCUSTUSERNAME ()
    {
        return CUSTUSERNAME;
    }

    public void setCUSTUSERNAME (String CUSTUSERNAME)
    {
        this.CUSTUSERNAME = CUSTUSERNAME;
    }

    public String getMERCHANTID ()
    {
        return MERCHANTID;
    }

    public void setMERCHANTID (String MERCHANTID)
    {
        this.MERCHANTID = MERCHANTID;
    }

    public String getCHANNELID ()
    {
        return CHANNELID;
    }

    public void setCHANNELID (String CHANNELID)
    {
        this.CHANNELID = CHANNELID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CUSTPROFILEPASSWORD = "+CUSTPROFILEPASSWORD+", CUSTUSERNAME = "+CUSTUSERNAME+", MERCHANTID = "+MERCHANTID+", CHANNELID = "+CHANNELID+"]";
    }
}