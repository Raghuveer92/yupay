package simplifii.framework.soap.requests;

public class SecurityAnswer
{
    private String QUESTIONID;

    private String ANSWER;

    public SecurityAnswer(){}

    public SecurityAnswer(String QUESTIONID, String ANSWER) {
        this.QUESTIONID = QUESTIONID;
        this.ANSWER = ANSWER;
    }

    public String getQUESTIONID ()
    {
        return QUESTIONID;
    }

    public void setQUESTIONID (String QUESTIONID)
    {
        this.QUESTIONID = QUESTIONID;
    }

    public String getANSWER ()
    {
        return ANSWER;
    }

    public void setANSWER (String ANSWER)
    {
        this.ANSWER = ANSWER;
    }

    @Override
    public String toString() {
        return "SecurityAnswer{" +
                "QUESTIONID='" + QUESTIONID + '\'' +
                ", ANSWER='" + ANSWER + '\'' +
                '}';
    }
}