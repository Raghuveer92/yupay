package simplifii.framework.soap.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidateOTPRequest {

    @Expose
    @SerializedName(value = "OTPREQUESTNO")
    private String otpRequestNo;

    @Expose
    @SerializedName(value = "TERMINALID")
    private String terminalId="1";

    @Expose
    @SerializedName(value = "TRANSACTIONTYPE")
    private String transactionType = "IP";

    @Expose
    @SerializedName(value = "MERCHANTID")
    private String merchantId ="57";

    @Expose
    @SerializedName(value = "CHANNELID")
    private String channelId ="2";

    @Expose
    @SerializedName(value = "OTP")
    private String otp;

    public String getOtpRequestNo() {
        return otpRequestNo;
    }

    public void setOtpRequestNo(String otpRequestNo) {
        this.otpRequestNo = otpRequestNo;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        return "ValidateOTPRequest{" +
                "otpRequestNo='" + otpRequestNo + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", channelId='" + channelId + '\'' +
                ", otp='" + otp + '\'' +
                '}';
    }
}
