package simplifii.framework.soap.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 9/26/2016.
 */
public class GenerateOTP {

    @Expose
    @SerializedName(value = "GENERATEOTPREQUEST")
    private GenerateOTPRequest generateOTPRequest;

    public GenerateOTPRequest getGenerateOTPRequest() {
        return generateOTPRequest;
    }

    public void setGenerateOTPRequest(GenerateOTPRequest generateOTPRequest) {
        this.generateOTPRequest = generateOTPRequest;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [GENERATEOTPREQUEST = "+generateOTPRequest+"]";
    }
}
