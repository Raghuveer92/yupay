package simplifii.framework.soap.requests;

import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;

import pt.joaocruz04.lib.annotations.JSoapClass;
import pt.joaocruz04.lib.annotations.JSoapReqField;
import pt.joaocruz04.lib.misc.SOAPSerializable;
import simplifii.framework.utility.Serializer;

/**
 * Created by Dell on 9/25/2016.
 */
@JSoapClass(namespace = "http://tempuri.org/")
public class GetSecurityQuestions implements Serializable{

    @JSoapReqField(fieldName = "xml", order = 0)
    private String xml;

    public GetSecurityQuestions(){
    }

    public GetSecurityQuestions(String xml){
        this.xml = xml;
    }

    @Override
    public String toString() {
        return "GetSecurityQuestions{" +
                "xml='" + xml + '\'' +
                '}';
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

}
