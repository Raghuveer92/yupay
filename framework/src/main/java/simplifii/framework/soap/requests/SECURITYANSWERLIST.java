package simplifii.framework.soap.requests;

import java.util.List;

public class SecurityAnswerList
{
    private List<SecurityAnswer> SECURITYANSWER;

    public List<SecurityAnswer> getSECURITYANSWER ()
    {
        return SECURITYANSWER;
    }

    public void setSECURITYANSWER (List<SecurityAnswer> SECURITYANSWER)
    {
        this.SECURITYANSWER = SECURITYANSWER;
    }

    @Override
    public String toString() {
        return "SecurityAnswerList{" +
                "SECURITYANSWER=" + SECURITYANSWER +
                '}';
    }
}