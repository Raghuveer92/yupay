package simplifii.framework.soap.requests;

public class RegisterCustomerRequest
{
    private String CUSTFULLNAME;

    private String HASHKEYS;

    private String CUSTPREFERENCE="1";

    private String CUSTPROFILEPASSWORD;

    private String CUSTUSERNAME;

    private SecurityAnswerList SECURITYANSWERLIST;

    private String CUSTEMAILID;

    private String CUSTMOBILENO;

    public String getCUSTFULLNAME ()
    {
        return CUSTFULLNAME;
    }

    public void setCUSTFULLNAME (String CUSTFULLNAME)
    {
        this.CUSTFULLNAME = CUSTFULLNAME;
    }

    public String getHASHKEYS ()
    {
        return HASHKEYS;
    }

    public void setHASHKEYS (String HASHKEYS)
    {
        this.HASHKEYS = HASHKEYS;
    }

    public String getCUSTPREFERENCE ()
    {
        return CUSTPREFERENCE;
    }

    public void setCUSTPREFERENCE (String CUSTPREFERENCE)
    {
        this.CUSTPREFERENCE = CUSTPREFERENCE;
    }

    public String getCUSTPROFILEPASSWORD ()
    {
        return CUSTPROFILEPASSWORD;
    }

    public void setCUSTPROFILEPASSWORD (String CUSTPROFILEPASSWORD)
    {
        this.CUSTPROFILEPASSWORD = CUSTPROFILEPASSWORD;
    }

    public String getCUSTUSERNAME ()
    {
        return CUSTUSERNAME;
    }

    public void setCUSTUSERNAME (String CUSTUSERNAME)
    {
        this.CUSTUSERNAME = CUSTUSERNAME;
    }

    public SecurityAnswerList getSECURITYANSWERLIST ()
    {
        return SECURITYANSWERLIST;
    }

    public void setSECURITYANSWERLIST (SecurityAnswerList SECURITYANSWERLIST)
    {
        this.SECURITYANSWERLIST = SECURITYANSWERLIST;
    }

    public String getCUSTEMAILID ()
    {
        return CUSTEMAILID;
    }

    public void setCUSTEMAILID (String CUSTEMAILID)
    {
        this.CUSTEMAILID = CUSTEMAILID;
    }

    public String getCUSTMOBILENO ()
    {
        return CUSTMOBILENO;
    }

    public void setCUSTMOBILENO (String CUSTMOBILENO)
    {
        this.CUSTMOBILENO = CUSTMOBILENO;
    }

    @Override
    public String toString() {
        return "RegisterCustomerRequest{" +
                "CUSTFULLNAME='" + CUSTFULLNAME + '\'' +
                ", HASHKEYS='" + HASHKEYS + '\'' +
                ", CUSTPREFERENCE='" + CUSTPREFERENCE + '\'' +
                ", CUSTPROFILEPASSWORD='" + CUSTPROFILEPASSWORD + '\'' +
                ", CUSTUSERNAME='" + CUSTUSERNAME + '\'' +
                ", SECURITYANSWERLIST=" + SECURITYANSWERLIST +
                ", CUSTEMAILID='" + CUSTEMAILID + '\'' +
                ", CUSTMOBILENO='" + CUSTMOBILENO + '\'' +
                '}';
    }
}