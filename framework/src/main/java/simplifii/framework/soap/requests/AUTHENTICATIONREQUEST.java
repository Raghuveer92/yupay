package simplifii.framework.soap.requests;

public class AuthenticationRequest
{
    private String USERNAME;

    private String PASSWORD;

    private String MERCHANTID;

    private String CHANNELID;

    public String getUSERNAME ()
    {
        return USERNAME;
    }

    public void setUSERNAME (String USERNAME)
    {
        this.USERNAME = USERNAME;
    }

    public String getPASSWORD ()
    {
        return PASSWORD;
    }

    public void setPASSWORD (String PASSWORD)
    {
        this.PASSWORD = PASSWORD;
    }

    public String getMERCHANTID ()
    {
        return MERCHANTID;
    }

    public void setMERCHANTID (String MERCHANTID)
    {
        this.MERCHANTID = MERCHANTID;
    }

    public String getCHANNELID ()
    {
        return CHANNELID;
    }

    public void setCHANNELID (String CHANNELID)
    {
        this.CHANNELID = CHANNELID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [USERNAME = "+USERNAME+", PASSWORD = "+PASSWORD+", MERCHANTID = "+MERCHANTID+", CHANNELID = "+CHANNELID+"]";
    }
}
