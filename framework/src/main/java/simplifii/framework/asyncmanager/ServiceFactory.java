package simplifii.framework.asyncmanager;

import android.content.Context;

import simplifii.framework.utility.AppConstants;


public class ServiceFactory {

    public static Service getInstance(Context context, int taskCode) {
        Service service = null;
        switch (taskCode) {
            case AppConstants.TASKCODES.ENCRYPT_PASSWORD:
                service = new HttpRestService();
                break;
            default:
                service = new XMLRestService(context);
                break;

        }
        return service;
    }

}
