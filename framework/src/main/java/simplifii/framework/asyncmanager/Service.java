package simplifii.framework.asyncmanager;


import simplifii.framework.exceptionhandler.RestException;

import org.json.JSONException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

public interface Service {

    public Object getData(Object... params) throws JSONException, SQLException, NullPointerException, RestException, ClassCastException, IOException, TransformerException, ParserConfigurationException, SAXException;


}
