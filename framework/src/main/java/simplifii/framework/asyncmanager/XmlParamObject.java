package simplifii.framework.asyncmanager;

import simplifii.framework.utility.SoapApiUtils;

/**
 * Created by Dell on 9/25/2016.
 */
public class XmlParamObject {

    private String actionName;
    private String json;
    private String assetFileName = SoapApiUtils.SOAP_BASE_API_FILE_NAME;
    private Class classType;
    private String nodeNameToParse;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getAssetFileName() {
        return assetFileName;
    }

    public void setAssetFileName(String assetFileName) {
        this.assetFileName = assetFileName;
    }

    public Class getClassType() {
        return classType;
    }

    public void setClassType(Class classType) {
        this.classType = classType;
    }

    public String getNodeNameToParse() {
        return nodeNameToParse;
    }

    public void setNodeNameToParse(String nodeNameToParse) {
        this.nodeNameToParse = nodeNameToParse;
    }
}
