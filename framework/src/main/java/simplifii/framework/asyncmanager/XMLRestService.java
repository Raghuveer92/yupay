package simplifii.framework.asyncmanager;

import android.content.Context;
import android.text.TextUtils;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.XML;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.DomParserUtils;
import simplifii.framework.utility.SoapApiUtils;
import simplifii.framework.utility.StreamUtils;

/**
 * Created by Dell on 9/26/2016.
 */
public class XMLRestService extends GenericService {

    private Context ctx;

    public XMLRestService(Context ctx){
        this.ctx = ctx;
    }

    @Override
    public Object getData(Object... params) throws JSONException, SQLException, NullPointerException, RestException, ClassCastException, IOException, TransformerException, ParserConfigurationException, SAXException {
        if(params!=null&&params.length>0) {
            XmlParamObject obj = (XmlParamObject) params[0];
            String soapBaseRequest = StreamUtils.convertAssetFileToString(ctx, obj.getAssetFileName());
            String cdataBody = "";
            if (!TextUtils.isEmpty(obj.getJson())) {
                cdataBody = StreamUtils.convertJsonStringToXmlString(obj.getJson());
            }
            String xmlRequest = soapBaseRequest.replace(SoapApiUtils.SOAP_BASE_API_METHOD_PLACEHOLDER, obj.getActionName()).replace(SoapApiUtils.SOAP_BASE_API_BODY_PLACEHOLDER, cdataBody);
            URL url = new URL(SoapApiUtils.API_URL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.addRequestProperty("Content-Type", SoapApiUtils.API_CONTENT_TYPE );
            conn.addRequestProperty("Host", SoapApiUtils.API_HOST);


            conn.addRequestProperty("Action", SoapApiUtils.API_ACTION +obj.getActionName());
            conn.addRequestProperty("Content-Length", xmlRequest.length() + "");

            conn.connect();
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            out.write(xmlRequest);// request hit
            out.close();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();
                String response = StreamUtils.convertStreamToString(is);
                String parsedResponse = DomParserUtils.parseXmlToNodeName(response, obj.getNodeNameToParse());
                return parseJson(XML.toJSONObject(parsedResponse).toString(), obj);
            }
        }
        return null;
    }

    protected Object parseJson(String jsonString, XmlParamObject postObject) {
        if (postObject.getClassType() == null) {
            return jsonString;
        }
        try {
            Class classType = postObject.getClassType();
            Method m = classType.getDeclaredMethod("parseJson", String.class);
            Object o = m.invoke(null, jsonString);

            return o;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // Parse with Gson
        return new Gson().fromJson(jsonString, postObject.getClassType());
    }
}
