package simplifii.framework.receivers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONObject;

import simplifii.framework.R;
import simplifii.framework.activity.PushNotification;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private Bundle data = new Bundle();
    private String title, message;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, "GCM Received");
        if (data != null) {
            for (String s : data.keySet()) {
                Log.d(s, "" + data.get(s) + "");
            }
        }
        this.data = data;
        String message = data.getString("message");

        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
//        String userId = Preferences.getData(AppConstants.PREF_KEYS.USER_ID, "");
//        boolean isSocialLogin = Preferences.getData(AppConstants.PREF_KEYS.SOCIAL_LOGIN, false);
//        if (!TextUtils.isEmpty(userId) || isSocialLogin) {
//            sendNotification(message);
//        }
        sendNotification(message);
        // [END_EXCLUDE]
    }

    protected int getSmallIcon() {
        return R.drawable.common_google_signin_btn_icon_dark;
    }

    // [END receive_message]
    protected Class getClassType() {
    return PushNotification.class;
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        Intent intent = new Intent(this, getClassType());
        if (data != null) {
            intent.putExtras(data);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String msg = getMessageFromData(data);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(getSmallIcon())
                .setColor(getResources().getColor(R.color.color_blue_bg))
                .setContentTitle(title)
                .setContentText(msg)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationCompat.BigTextStyle compat = new NotificationCompat.BigTextStyle();
        compat.setBigContentTitle(title);
//        compat.setSummaryText(msg);
        compat.bigText(msg);
//        compat.setBuilder(notificationBuilder);
        notificationBuilder.setStyle(compat);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private String getMessageFromData(Bundle data) {
        String json = data.getString("Response");
        try {
            JSONObject obj = new JSONObject(json);
            JSONObject notifyResponse = obj.optJSONObject("notifyResponse");
            message = notifyResponse.optString("message");
            title = notifyResponse.optString("title");
            return message;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}