package com.example.neerajyadav.yupay.model;

/**
 * Created by Neeraj Yadav on 9/17/2016.
 */
public class Billers {
    String bTitle, bId,bAmount,bRef,bDate;
    int logo, menu;

    public String getbTitle() {
        return bTitle;
    }

    public void setbTitle(String bTitle) {
        this.bTitle = bTitle;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }

    public String getbAmount() {
        return bAmount;
    }

    public void setbAmount(String bAmount) {
        this.bAmount = bAmount;
    }

    public String getbRef() {
        return bRef;
    }

    public void setbRef(String bRef) {
        this.bRef = bRef;
    }

    public String getbDate() {
        return bDate;
    }

    public void setbDate(String bDate) {
        this.bDate = bDate;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public int getMenu() {
        return menu;
    }

    public void setMenu(int menu) {
        this.menu = menu;
    }
}
