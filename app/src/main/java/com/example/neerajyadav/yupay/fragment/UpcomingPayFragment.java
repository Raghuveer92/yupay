package com.example.neerajyadav.yupay.fragment;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.model.ReminderDescription;
import com.example.neerajyadav.yupay.model.ReminderModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.fragments.BaseFragment;

public class UpcomingPayFragment extends BaseFragment {
    private LinearLayout lay_payment;
    private LayoutInflater inflater;
    ReminderModel reminderModel;


    @Override
    public void initViews() {
        initData();
        lay_payment = (LinearLayout) findView(R.id.lay_payment);
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setData(reminderModel);
        setData(reminderModel);

    }

    private void setData(ReminderModel reminderModel) {
        View view = inflater.inflate(R.layout.row_payment_title, null);
        setText(R.id.tv_title,reminderModel.getTitle(),view);
        lay_payment.addView(view);
        for(ReminderDescription reminderDescription:reminderModel.getReminderDescriptionList()){
            View viewReminderDescription = inflater.inflate(R.layout.row_payment_desc, null);
            setText(R.id.tv_title,reminderDescription.getTitle(),viewReminderDescription);
            lay_payment.addView(viewReminderDescription);
        }

    }

    private void initData() {
        reminderModel=new ReminderModel();
        reminderModel.setTitle("Today");
        List<ReminderDescription> reminderDescriptions=new ArrayList<>();
        for(int x=0;x<3;x++){
            ReminderDescription reminderDescription=new ReminderDescription();
            reminderDescription.setTitle("Aramax "+x);
            reminderDescription.setSubTitle("7891332878");
            reminderDescription.setAmount("958");
            reminderDescriptions.add(reminderDescription);
        }
        reminderModel.setReminderDescriptionList(reminderDescriptions);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_upcoming_pay;
    }
}
