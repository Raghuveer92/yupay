package com.example.neerajyadav.yupay.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.neerajyadav.yupay.activity.HistoryActivity;
import com.example.neerajyadav.yupay.activity.ProfileSettingActivity;
import com.example.neerajyadav.yupay.R;

import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Neeraj Yadav on 9/15/2016.
 */
public class AccountSettingFragment extends BaseFragment implements CustomListAdapterInterface {

    private LinearLayout linearLayout;
    private String[] name;
    private LayoutInflater inflater;

    public static AccountSettingFragment getInstance() {
        return new AccountSettingFragment();
    }

    @Override
    public void initViews() {
        initToolBar("");
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        linearLayout = (LinearLayout) findView(R.id.lv_account_setting);
        name = new String[]{"Payment History","Activity History","Choose Language","About YuPay","Help & FAQs","Terms & Conditions"};
        setHeader();
        setRow();
    }

    private void setHeader() {
        View view = inflater.inflate(R.layout.row_user_account, null);
        linearLayout.addView(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNextActivity(ProfileSettingActivity.class);
            }
        });
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.bell;
    }

    private void setRow() {
        for (int x=0;x<name.length;x++) {
            View view = inflater.inflate(R.layout.row_account_menu, null);
            setText(R.id.tv_u_name, name[x], view);
            linearLayout.addView(view);
            final int finalX = x;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (finalX){
                        case 0:
                            break;
                        case 1:
                            startNextActivity(HistoryActivity.class);
                            break;
                    }
                }
            });
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_account_setting;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        return null;
    }
}
