package com.example.neerajyadav.yupay;

import android.app.Application;

import simplifii.framework.utility.Preferences;

/**
 * Created by Dell on 9/27/2016.
 */
public class AppController extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Preferences.initSharedPreferences(this);
    }
}
