package com.example.neerajyadav.yupay.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.example.neerajyadav.yupay.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 9/20/2016.
 */
public class ScheduleReminderFragment extends BaseFragment {
    private LinearLayout lay_top;
    private RelativeLayout rl_pay_date;
    private EditText et_pay_date;
    private TextInputLayout til_pay_date;
    private int mYear, mMonth, mDay;
    private Button btn_add_reminder;
    OnSelectReminder onSelectReminder;

    public static ScheduleReminderFragment getInstance(OnSelectReminder onSelectReminder) {
        ScheduleReminderFragment instance=new ScheduleReminderFragment();
        instance.onSelectReminder=onSelectReminder;
        return instance;
    }

    @Override
    public void initViews() {
        rl_pay_date = (RelativeLayout) findView(R.id.rl_pay_date);
        lay_top = (LinearLayout) findView(R.id.lay_top);
        et_pay_date = (EditText) findView(R.id.et_pay_date);
        til_pay_date = (TextInputLayout) findView(R.id.til_pay_date);
        btn_add_reminder = (Button) findView(R.id.btn_add_reminder);
        setOnClickListener(R.id.lay_top, R.id.rl_pay_date, R.id.btn_add_reminder);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lay_top:
                break;
            case R.id.rl_pay_date:
                payDate();
                break;
            case R.id.btn_add_reminder:

                String date_time = til_pay_date.getEditText().getText().toString().trim();
                if (TextUtils.isEmpty(date_time))
                {
                    til_pay_date.setError(getString(R.string.cannot_empty));
                    return;
                }else {
                    et_pay_date.setError(null);
                }
                onSelectReminder.onSelectDate(date_time);
                getActivity().onBackPressed();
                break;
        }
    }

    private void payDate() {
        final Calendar c = Calendar.getInstance();
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mMonth = c.get(Calendar.MONTH);
        mYear = c.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                c.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy");
                String format = simpleDateFormat.format(c.getTime());
                et_pay_date.setText(format);
                payTime();

            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    private void payTime() {
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                mcurrentTime.set(selectedHour,selectedMinute);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm a");
                String format = simpleDateFormat.format(mcurrentTime.getTime());
                et_pay_date.setText(et_pay_date.getText()+ " at " +format);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_schedule_reminder;
    }

    public interface OnSelectReminder {
        void onSelectDate(String time);
    }
}
