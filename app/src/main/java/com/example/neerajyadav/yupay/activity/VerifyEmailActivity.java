package com.example.neerajyadav.yupay.activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.XmlParamObject;
import simplifii.framework.rest.requests.EncryptPasswordRequest;
import simplifii.framework.rest.responses.EncryptedPasswordResponse;
import simplifii.framework.soap.requests.GenerateOTP;
import simplifii.framework.soap.requests.GenerateOTPRequest;
import simplifii.framework.soap.requests.ValidateOTPRequest;
import simplifii.framework.soap.requests.ValidateOtp;
import simplifii.framework.soap.responses.GenerateOTPResult;
import simplifii.framework.soap.responses.ValidateOTPResponse;
import simplifii.framework.soap.responses.ValidateOTPResult;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.SoapApiUtils;
import simplifii.framework.utility.StreamUtils;

public class VerifyEmailActivity extends BaseActivity {
    private TextInputLayout til_otp;
    private Button btn_verify;
    private TextView tv_otp_mail;
    private TextView tv_edit_email, tv_resend;
    private String otpRequestNumber, otpEntered;
    private boolean otpGenerated = false;
    private String password,email,number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);

        initToolBar("");

        til_otp = (TextInputLayout) findViewById(R.id.til_otp);
        btn_verify = (Button) findViewById(R.id.btn_verify);
        tv_edit_email = (TextView) findViewById(R.id.tv_edit_email);
        tv_resend = (TextView) findViewById(R.id.tv_resend);
        tv_edit_email.setText(R.string.edit);
        tv_otp_mail = (TextView) findViewById(R.id.tv_otp_email);

        setOnClickListener(R.id.btn_verify, R.id.tv_edit_email, R.id.tv_resend);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            email = bundle.getString(SoapApiUtils.KEY_EMAIL);
            tv_otp_mail.setText(email);
            number = bundle.getString(SoapApiUtils.KEY_MOBILE);
            password = bundle.getString(SoapApiUtils.KEY_PASSWORD);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        generateOtp();
    }

    private void generateOtp() {
        if (!TextUtils.isEmpty(email)) {
            GenerateOTP generateOTP = new GenerateOTP();
            GenerateOTPRequest generateOTPRequest = new GenerateOTPRequest();
            generateOTPRequest.setCustEmail(email);
            generateOTPRequest.setCustMobile(number);
            generateOTP.setGenerateOTPRequest(generateOTPRequest);
            XmlParamObject xmlParamObject = new XmlParamObject();
            xmlParamObject.setActionName(SoapApiUtils.ACTION_GENERATE_OTP);
            xmlParamObject.setJson(JsonUtil.getJsonString(generateOTP));
            xmlParamObject.setNodeNameToParse(SoapApiUtils.NODE_NAME_GENERATE_OTP);
            xmlParamObject.setClassType(GenerateOTPResult.class);
            executeTask(AppConstants.TASKCODES.GENERATE_OTP, xmlParamObject);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_verify:
                verify();
                break;
            case R.id.tv_edit_email:
                forgotpass();
                break;
            case R.id.tv_resend:
                resend();
                break;
        }
    }

    private void resend() {
        showToast("Resent Successfully");
    }

    private void forgotpass() {
        startNextActivity(ForgotPasswordActivity.class);
    }

    private void verify() {
        String otp = til_otp.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(otp)) {
            til_otp.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_otp.setError(null);
        }
        otpEntered = otp;
        if (!otpGenerated) {
            showToast("OTP not yet generated");
        }
        ValidateOtp validateOtp = new ValidateOtp();
        ValidateOTPRequest request = new ValidateOTPRequest();
        request.setOtpRequestNo(otpRequestNumber);
        request.setOtp(otp);
        validateOtp.setValidateOTPRequest(request);
        XmlParamObject xmlParamObject = new XmlParamObject();
        xmlParamObject.setActionName(SoapApiUtils.ACTION_VALIDATE_OTP);
        xmlParamObject.setClassType(ValidateOTPResult.class);
        xmlParamObject.setJson(JsonUtil.getJsonString(validateOtp));
        xmlParamObject.setNodeNameToParse(SoapApiUtils.NODE_NAME_VALIDATE_OTP);
        executeTask(AppConstants.TASKCODES.VALIDATE_OTP, xmlParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GENERATE_OTP:
                GenerateOTPResult result = (GenerateOTPResult) response;
                if (result != null && result.getOtpresponse() != null) {
                    otpRequestNumber = result.getOtpresponse().getOtpRequestNo();
                    otpGenerated = true;
                    showToast("OTP generated successfully");
                }
                break;
            case AppConstants.TASKCODES.VALIDATE_OTP:
                ValidateOTPResult validateOTPResult = (ValidateOTPResult) response;
                if (validateOTPResult != null && validateOTPResult.getValidateOTPResponse() != null) {
                    if ("true".equalsIgnoreCase(validateOTPResult.getValidateOTPResponse().getIsValid())) {
                        showToast(validateOTPResult.getValidateOTPResponse().getResponseMessage());
                        generateEncryptedPassword();
                    } else if ("FALSE".equalsIgnoreCase(validateOTPResult.getValidateOTPResponse().getIsValid())) {
                        showToast(validateOTPResult.getValidateOTPResponse().getResponseMessage());
                    }
                }
                break;

            case AppConstants.TASKCODES.ENCRYPT_PASSWORD :
                EncryptedPasswordResponse encryptedPasswordResponse = (EncryptedPasswordResponse) response;
                if(encryptedPasswordResponse!=null&&encryptedPasswordResponse.getHashValueResponse()!=null){
                    String encryptedPass = encryptedPasswordResponse.getHashValueResponse().getAesEncrytpedValues();
                    String hashkey = encryptedPasswordResponse.getHashValueResponse().getSaltValues();
                    Intent intent = new Intent();
                    intent.putExtra(SoapApiUtils.KEY_ENCRYPTED_PASSWORD, encryptedPass);
                    intent.putExtra(SoapApiUtils.KEY_HASHKEY, hashkey);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
        }
    }

    private void generateEncryptedPassword() {
        String toBeEncrypted = StreamUtils.getTokenForOtp(otpRequestNumber, otpEntered);
        EncryptPasswordRequest request = new EncryptPasswordRequest(toBeEncrypted,password);
        String json = JsonUtil.getJsonString(request);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setPostMethod();
        httpParamObject.addHeader(AppConstants.CONTENT_LENGTH,json.length()+"");
        httpParamObject.setClassType(EncryptedPasswordResponse.class);
        httpParamObject.setUrl(AppConstants.ENCRYPT_PASSWORD_URL);
        httpParamObject.setJson(json);
        executeTask(AppConstants.TASKCODES.ENCRYPT_PASSWORD,httpParamObject);
    }

}
