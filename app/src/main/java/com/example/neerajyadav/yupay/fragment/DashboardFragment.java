package com.example.neerajyadav.yupay.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;

import com.example.neerajyadav.yupay.R;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Neeraj Yadav on 9/21/2016.
 */

public class DashboardFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface {
    List<String> tab = new ArrayList<>();
    private ViewPager viewPager;

    public static DashboardFragment getInstance() {
        return new DashboardFragment();
    }

    @Override
    public void initViews() {
        initToolBar("");
        initTab();
        viewPager = (ViewPager) findView(R.id.viewpager);
        TabLayout tabLayout = (TabLayout) findView(R.id.tab_layout);
        CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(getChildFragmentManager(),tab,this);
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.bell;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_menu, menu);
    }

    private void initTab() {
        tab.add("Biller's Overview");
        tab.add("Payment History");
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_dashboard;
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position){
            case 0:
                DashboardBillersFragment dashboardBillersFragment = new DashboardBillersFragment();
                return dashboardBillersFragment;
            case 1:
                DashboardPaymentFragment dashboardPaymentFragment = new DashboardPaymentFragment();
                return dashboardPaymentFragment;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tab.get(position);
    }
}
