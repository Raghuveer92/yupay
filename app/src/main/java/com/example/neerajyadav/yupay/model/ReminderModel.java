package com.example.neerajyadav.yupay.model;

import java.util.List;

/**
 * Created by Neeraj Yadav on 9/16/2016.
 */
public class ReminderModel {
    String title;
    List<ReminderDescription> reminderDescriptionList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ReminderDescription> getReminderDescriptionList() {
        return reminderDescriptionList;
    }

    public void setReminderDescriptionList(List<ReminderDescription> reminderDescriptionList) {
        this.reminderDescriptionList = reminderDescriptionList;
    }
}
