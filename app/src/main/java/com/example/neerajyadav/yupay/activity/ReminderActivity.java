package com.example.neerajyadav.yupay.activity;

import android.app.DatePickerDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.neerajyadav.yupay.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import simplifii.framework.activity.BaseActivity;

import static android.R.attr.format;

public class ReminderActivity extends BaseActivity {
    private TextInputLayout til_pay_date;
    private RelativeLayout rl_pay_date;
    private EditText et_pay_date;
    private int mDay, mMonth, mYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        til_pay_date = (TextInputLayout) findViewById(R.id.til_pay_date);
        rl_pay_date = (RelativeLayout) findViewById(R.id.rl_pay_date);
        et_pay_date = (EditText) findViewById(R.id.et_pay_date);
        initToolBar("");
        
        setOnClickListener(R.id.rl_pay_date);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_pay_date:
                paymentDate();
        }
        super.onClick(v);
    }

    private void paymentDate() {
        final Calendar c = Calendar.getInstance();
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mMonth = c.get(Calendar.MONTH);
        mYear = c.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(ReminderActivity.this, new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                c.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy");
                String format = simpleDateFormat.format(c.getTime());
                et_pay_date.setText(format);

            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }
}
