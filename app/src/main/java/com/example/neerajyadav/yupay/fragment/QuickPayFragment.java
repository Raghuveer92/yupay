package com.example.neerajyadav.yupay.fragment;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.neerajyadav.yupay.R;

import simplifii.framework.fragments.BaseFragment;

public class QuickPayFragment extends BaseFragment {


    public static QuickPayFragment getInstance() {
        return new QuickPayFragment();
    }

    @Override
    public void initViews() {
        initToolBar("");

    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.bell;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_quick_pay;
    }
}
