package com.example.neerajyadav.yupay.activity;

import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.Util;

public class ForgotPasswordActivity extends BaseActivity {
    private TextInputLayout til_user_id,til_sec_ans1,til_sec_ans2;
    private Button btn_login;
    private TextView tv_sign_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initToolBar("");
        til_user_id = (TextInputLayout) findViewById(R.id.til_user_id);
        til_sec_ans1 = (TextInputLayout) findViewById(R.id.til_sec_ans1);
        til_sec_ans2 = (TextInputLayout) findViewById(R.id.til_sec_ans2);
        btn_login = (Button) findViewById(R.id.btn_login);
        tv_sign_up = (TextView) findViewById(R.id.tv_sign_up);

        setOnClickListener(R.id.btn_login,R.id.tv_sign_up);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_login:
                submitDetail();
                break;
            case R.id.tv_sign_up:
                register();
                break;
        }
    }

    private void submitDetail() {
        String userId = til_user_id.getEditText().getText().toString().trim();
        String sa1 = til_sec_ans1.getEditText().getText().toString().trim();
        String sa2 = til_sec_ans2.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(userId)) {
            til_user_id.setError(getString(R.string.cannot_empty));
            return;
        } else if (!Util.isValidEmail(userId)) {
            til_user_id.setError(getString(R.string.invalid_email));
            return;
        } else {
            til_user_id.setError(null);
        }
        if (TextUtils.isEmpty(sa1)){
            til_sec_ans1.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_sec_ans1.setError(null);
        }
        if (TextUtils.isEmpty(sa2)){
            til_sec_ans2.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_sec_ans2.setError(null);
        }
        showToast("Password send to your Email Id");
    }

    private void register() {
        startNextActivity(SignUpActivity.class);
    }

}
