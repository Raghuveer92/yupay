package com.example.neerajyadav.yupay.fragment;

import android.support.v4.app.Fragment;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.neerajyadav.yupay.R;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Neeraj Yadav on 9/20/2016.
 */
public class QuickpayNotsign2Fragment extends BaseFragment {
    private Spinner spinner_amount;

    public static QuickpayNotsign2Fragment getInstance() {
        return new QuickpayNotsign2Fragment();
    }

    @Override
    public void initViews() {
        spinner_amount = (Spinner) findView(R.id.spinner_amount);
        List<String> currency = new ArrayList<String>();
        currency.add("Rs");
        currency.add("AED");
        currency.add("DOL");
        currency.add("Pond");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,currency);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_amount.setAdapter(dataAdapter);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_quickpay_notsign2;
    }
}
