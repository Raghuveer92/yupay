package com.example.neerajyadav.yupay.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.fragment.AccountSettingFragment;
import com.example.neerajyadav.yupay.fragment.BillersFragment;
import com.example.neerajyadav.yupay.fragment.DashboardFragment;
import com.example.neerajyadav.yupay.fragment.QuickPayFragment;
import com.example.neerajyadav.yupay.fragment.QuickpayNotsign2Fragment;
import com.example.neerajyadav.yupay.fragment.ReminderFragment;

import simplifii.framework.activity.BaseActivity;

import static com.example.neerajyadav.yupay.R.id.iv_billers;
import static com.example.neerajyadav.yupay.R.id.iv_more;

public class AccountSettingActivity extends BaseActivity {
    ImageView iv_dashboard, iv_quickpay, iv_billers, iv_reminder, iv_more;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setting);
        iv_dashboard = (ImageView) findViewById(R.id.iv_dashboard);
        iv_quickpay = (ImageView) findViewById(R.id.iv_quickpay);
        iv_billers = (ImageView) findViewById(R.id.iv_billers);
        iv_reminder = (ImageView) findViewById(R.id.iv_reminder);
        iv_more = (ImageView) findViewById(R.id.iv_more);
        addFragment(DashboardFragment.getInstance(),false);
        iv_dashboard.setColorFilter(getResourceColor(R.color.tab_blue));
        setOnClickListener(R.id.iv_dashboard,R.id.iv_quickpay, R.id.iv_billers,R.id.iv_reminder,R.id.iv_more);

    }
    private void addFragment(Fragment fragment,boolean addToBackStack){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(addToBackStack){
            fragmentTransaction.addToBackStack("");
        }
        fragmentTransaction.replace(R.id.fl_ac,fragment).commit();
    }
    public void onClick(View v){
        setUnselectAll();
        switch (v.getId()){
            case R.id.iv_dashboard:
                addFragment(DashboardFragment.getInstance(),false);
                setSelect(iv_dashboard);
                break;
            case R.id.iv_quickpay:
                addFragment(QuickPayFragment.getInstance(),false);
                setSelect(iv_quickpay);
                break;
            case R.id.iv_billers:
                addFragment(BillersFragment.getInstance(), false);
                setSelect(iv_billers);
                break;
            case R.id.iv_reminder:
                addFragment(ReminderFragment.getInstance(), false);
                setSelect(iv_reminder);
                break;
            case R.id.iv_more:
                addFragment(AccountSettingFragment.getInstance(), false);
                setSelect(iv_more);
                break;
        }
    }

    private void setUnselectAll() {
        iv_dashboard.setColorFilter(getResourceColor(R.color.tab_icon));
        iv_quickpay.setColorFilter(getResourceColor(R.color.tab_icon));
        iv_billers.setColorFilter(getResourceColor(R.color.tab_icon));
        iv_reminder.setColorFilter(getResourceColor(R.color.tab_icon));
        iv_more.setColorFilter(getResourceColor(R.color.tab_icon));
    }

    private void setSelect(ImageView iv) {
     iv.setColorFilter(getResourceColor(R.color.tab_blue));
    }
}
