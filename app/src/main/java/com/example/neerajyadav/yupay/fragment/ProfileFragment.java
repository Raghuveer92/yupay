package com.example.neerajyadav.yupay.fragment;

import android.app.DatePickerDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.activity.QuickpayReminderActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.soap.responses.ValidateCustomerResponse;
import simplifii.framework.soap.responses.ValidateCustomerResult;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class ProfileFragment extends BaseFragment {
    private RelativeLayout rl_dob;
    private TextInputLayout til_name, til_email, til_mobile, til_dob, til_address, til_emirates_id;
    private TextView tv_verify;
    private Button btn_update;
    private Spinner spinner_country, spinner_city;
    private EditText et_dob;
    private int mYear, mMonth, mDay;

    @Override
    public void initViews() {
        spinner_country= (Spinner) findView(R.id.spinner_country);
        spinner_city= (Spinner) findView(R.id.spinner_city);

        rl_dob = (RelativeLayout) findView(R.id.rl_dob);

        til_name = (TextInputLayout) findView(R.id.til_name);
        til_email = (TextInputLayout) findView(R.id.til_email);
        til_dob = (TextInputLayout) findView(R.id.til_dob);
        til_mobile = (TextInputLayout) findView(R.id.til_mobile);
        til_address = (TextInputLayout) findView(R.id.til_address);
        til_emirates_id = (TextInputLayout) findView(R.id.til_emirates_id);

        et_dob = (EditText) findView(R.id.et_dob);

        btn_update = (Button) findView(R.id.btn_update);

        tv_verify = (TextView) findView(R.id.tv_verify);

        setDefaultData();

        List<String> country = new ArrayList<String>();
        country.add("India");
        country.add("UAE");
        country.add("Australia");
        country.add("Canada");
        country.add("Yurope");

        List<String> city = new ArrayList<String>();
        city.add("Delhi");
        city.add("Mumbai");
        city.add("Chennai");
        city.add("Banglore");
        city.add("Kolkata");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,country);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_country.setAdapter(dataAdapter);

        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,city);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_city.setAdapter(cityAdapter);

        setOnClickListener(R.id.btn_update,R.id.tv_verify, R.id.rl_dob);
    }

    private void setDefaultData() {
        ValidateCustomerResult runningInstance = ValidateCustomerResult.getRunningInstance();
        if(runningInstance!=null&&runningInstance.getVALIDATECUSTOMERRESPONSE()!=null){
            ValidateCustomerResponse savedResposne = runningInstance.getVALIDATECUSTOMERRESPONSE();
            til_email.getEditText().setText(savedResposne.getCUSTEMAILID());
            til_mobile.getEditText().setText(savedResposne.getCUSTMOBILENO());
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_update:
                updateData();
                break;
            case R.id.tv_verify:
                startNextActivity(QuickpayReminderActivity.class);
                break;
            case R.id.rl_dob:
                datePicker();
                break;
        }
    }

    private void datePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {


                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year,monthOfYear,dayOfMonth);
                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd MMMM yyyy");
                        String format = simpleDateFormat.format(c.getTime());
                        et_dob.setText(format);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    private void updateData() {
        String name = til_name.getEditText().getText().toString().trim();
        String email = til_email.getEditText().getText().toString().trim();
        String mobile = til_mobile.getEditText().getText().toString().trim();
        String dob = til_dob.getEditText().getText().toString().trim();
        String address = til_address.getEditText().getText().toString().trim();
        String emirates_id = til_emirates_id.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            til_name.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_name.setError(null);
        }
        if (TextUtils.isEmpty(email)) {
            til_email.setError(getString(R.string.cannot_empty));
            return;
        } else if (!Util.isValidEmail(email)) {
            til_email.setError(getString(R.string.invalid_email));
            return;
        } else {
            til_email.setError(null);
        }
        if (mobile.length() < 10 ) {
            til_mobile.setError(getString(R.string.mnumber));
            return;
        } else {
            til_mobile.setError(null);
        }
        if (TextUtils.isEmpty(dob)){
            til_dob.setError(getString(R.string.cannot_empty));
            return;
        }else {
            til_dob.setError(null);
        }
        if (TextUtils.isEmpty(address)){
            til_address.setError(getString(R.string.cannot_empty));
            return;
        }else {
            til_address.setError(null);
        }
        if (TextUtils.isEmpty(emirates_id)){
            til_emirates_id.setError(getString(R.string.cannot_empty));
            return;
        }else {
            til_emirates_id.setError(null);
        }

        showToast("Successfully Update");

    }

}
