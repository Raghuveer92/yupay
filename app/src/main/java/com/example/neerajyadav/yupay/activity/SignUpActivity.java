package com.example.neerajyadav.yupay.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.XmlParamObject;
import simplifii.framework.rest.requests.EncryptPasswordRequest;
import simplifii.framework.rest.responses.EncryptedPasswordResponse;
import simplifii.framework.soap.requests.RegisterCustomer;
import simplifii.framework.soap.requests.RegisterCustomerRequest;
import simplifii.framework.soap.requests.SecurityAnswer;
import simplifii.framework.soap.requests.SecurityAnswerList;
import simplifii.framework.soap.responses.GetSecurityQuestionsResult;
import simplifii.framework.soap.responses.RegisterCustomerResult;
import simplifii.framework.soap.responses.SecurityQuestion;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionsUtils;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.SoapApiUtils;
import simplifii.framework.utility.StreamUtils;
import simplifii.framework.utility.Util;

public class SignUpActivity extends BaseActivity {
    private TextInputLayout til_email, til_password, til_country_code, til_number, til_sa1, til_sa2;
    private Button btn_signup;
    private TextView tv_skip, tv_log_in;
    private Spinner spinnerSQ1, spinnerSQ2;
    private Map<String, String> questionVsIdMap;
    private List<String> questionList;
    private ArrayAdapter<String> adapter1, adapter2;
    private String email,password,number;
    private String countryCode;
    private String secretAnswer1, secretAnswer2;
    private String selectedQuestion1Id, selectedQuestion2Id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        til_email = (TextInputLayout) findViewById(R.id.til_email);
        til_password = (TextInputLayout) findViewById(R.id.til_password);
        til_country_code = (TextInputLayout) findViewById(R.id.til_country_code);
        til_number = (TextInputLayout) findViewById(R.id.til_number);
        til_sa1 = (TextInputLayout) findViewById(R.id.til_sa1);
        til_sa2 = (TextInputLayout) findViewById(R.id.til_sa2);
        btn_signup = (Button) findViewById(R.id.btn_signup);
        setSpinners();
        tv_skip = (TextView) findViewById(R.id.tv_skip);
        tv_log_in = (TextView) findViewById(R.id.tv_log_in);
        setOnClickListener(R.id.btn_signup, R.id.tv_log_in, R.id.tv_skip);
        questionList = new ArrayList<>();
        setQuestionsData();
    }

    private void setQuestionsData() {

        adapter1 = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_list_item_1, questionList);
        spinnerSQ1.setAdapter(adapter1);
        adapter2 = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_list_item_1, questionList);
        spinnerSQ2.setAdapter(adapter2);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getSecQuestions();
    }

    private void getSecQuestions() {
        XmlParamObject object = new XmlParamObject();
        object.setClassType(GetSecurityQuestionsResult.class);
        object.setActionName(SoapApiUtils.ACTION_GET_SECURITY_QUESTIONS);
        object.setJson("");
        object.setNodeNameToParse(SoapApiUtils.NODE_NAME_GET_SECURITY_QUESTIONS);
        executeTask(AppConstants.TASKCODES.GET_SECURITY_QUESTIONS, object);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
                redirectToOtpScreen();
                break;
            case R.id.tv_log_in:
                setLogin();
                break;
            case R.id.tv_skip:
                verify();
                break;
        }
    }

    private void verify() {
        startNextActivity(VerifyEmailActivity.class);
    }

    private void setLogin() {
        startNextActivity(LoginActivity.class);
    }



    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_SECURITY_QUESTIONS:
                GetSecurityQuestionsResult result = (GetSecurityQuestionsResult) response;
                if (result != null) {
                    System.out.println(result.toString());
                    if (result.getSecurityQuestionDetailResponse() != null && "0".equals(result.getSecurityQuestionDetailResponse().getResponseCode()) && CollectionsUtils.isNotEmpty(result.getSecurityQuestionDetailResponse().getSecurityQuestionList())) {
                        questionVsIdMap = new HashMap<>();
                        for (SecurityQuestion securityQuestion : result.getSecurityQuestionDetailResponse().getSecurityQuestionList()) {
                            questionVsIdMap.put(securityQuestion.getQuestion(), securityQuestion.getId());
                        }
                        questionList.addAll(questionVsIdMap.keySet());
                        adapter1.notifyDataSetChanged();
                        adapter2.notifyDataSetChanged();
                    }
                }
                break;
            case AppConstants.TASKCODES.REGISTER :
                RegisterCustomerResult registerCustomerResult = (RegisterCustomerResult) response;
                if(registerCustomerResult!=null&&registerCustomerResult.getREGISTERCUSTOMERRESPONSE()!=null){

                    if("0".equals(registerCustomerResult.getREGISTERCUSTOMERRESPONSE().getRESPONSECODE())){
                        //startNextActivity
                        showToast(registerCustomerResult.getREGISTERCUSTOMERRESPONSE().getRESPONSEMESSAGE()+"!! Login to continue...");
                        startNextActivity(LoginActivity.class);
                        finish();
                    }else if("226".equals(registerCustomerResult.getREGISTERCUSTOMERRESPONSE().getRESPONSECODE())){
                        showToast(registerCustomerResult.getREGISTERCUSTOMERRESPONSE().getRESPONSEMESSAGE());
                        til_number.requestFocus();
                    }else{
                        showToast(registerCustomerResult.getREGISTERCUSTOMERRESPONSE().getRESPONSEMESSAGE());
                    }
                }
        }
    }

    private void registerCustomer(String encryptedPass, String hashkey) {
        RegisterCustomer registerCustomer = new RegisterCustomer();

        RegisterCustomerRequest registerCustomerRequest = new RegisterCustomerRequest();

        registerCustomerRequest.setCUSTEMAILID(email);
        registerCustomerRequest.setCUSTFULLNAME("");
        registerCustomerRequest.setCUSTMOBILENO(number);
        //registerCustomerRequest.setCUSTPREFERENCE(); Already set to 1
        registerCustomerRequest.setCUSTUSERNAME(email);
        registerCustomerRequest.setCUSTPROFILEPASSWORD(encryptedPass);
        registerCustomerRequest.setHASHKEYS(hashkey);

        SecurityAnswerList securityAnswerListObj = new SecurityAnswerList();

        List<SecurityAnswer> securityAnswerList = new ArrayList<>();

        SecurityAnswer securityAnswer1 = new SecurityAnswer(selectedQuestion1Id,secretAnswer1);
        SecurityAnswer securityAnswer2 = new SecurityAnswer(selectedQuestion2Id,secretAnswer2);
        securityAnswerList.add(securityAnswer1);
        securityAnswerList.add(securityAnswer2);

        securityAnswerListObj.setSECURITYANSWER(securityAnswerList);

        registerCustomerRequest.setSECURITYANSWERLIST(securityAnswerListObj);

        registerCustomer.setREGISTERCUSTOMERREQUEST(registerCustomerRequest);

        XmlParamObject xmlParamObject = new XmlParamObject();
        xmlParamObject.setActionName(SoapApiUtils.ACTION_REGISTER_USER);
        xmlParamObject.setJson(JsonUtil.getJsonString(registerCustomer));
        xmlParamObject.setNodeNameToParse(SoapApiUtils.NODE_NAME_REGISTER_CUSTOMER);
        xmlParamObject.setClassType(RegisterCustomerResult.class);
        executeTask(AppConstants.TASKCODES.REGISTER,xmlParamObject);
    }

    private void redirectToOtpScreen() {
        email = til_email.getEditText().getText().toString().trim();
        password = til_password.getEditText().getText().toString().trim();
        countryCode = til_country_code.getEditText().getText().toString().trim();
        number = til_number.getEditText().getText().toString().trim();
        secretAnswer1 = til_sa1.getEditText().getText().toString().trim();
        secretAnswer2 = til_sa2.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            til_email.setError(getString(R.string.cannot_empty));
            return;
        } else if (!Util.isValidEmail(email)) {
            til_email.setError(getString(R.string.invalid_email));
            return;
        } else {
            til_email.setError(null);
        }
        if (TextUtils.isEmpty(password)) {
            til_password.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_password.setError(null);
        }
        if (countryCode.length() < 2) {
            til_country_code.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_country_code.setError(null);
        }
        if (number.length() <= 9 || number.length() > 10) {
            til_number.setError(getString(R.string.mnumber));
            return;
        } else {
            til_number.setError(null);
        }

        if (spinnerSQ1.getSelectedItem().toString().equals(spinnerSQ2.getSelectedItem().toString())) {
            showToast("Please select different questions");
            return;
        }
        if (TextUtils.isEmpty(secretAnswer1)) {
            til_sa1.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_sa1.setError(null);
        }
        if (TextUtils.isEmpty(secretAnswer2)) {
            til_sa2.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_sa2.setError(null);
        }
        selectedQuestion1Id = questionVsIdMap.get(spinnerSQ1.getSelectedItem().toString());
        selectedQuestion2Id = questionVsIdMap.get(spinnerSQ2.getSelectedItem().toString());
        number = "+" + countryCode + number;
        showToast("Successfully Register");
        Bundle bundle = new Bundle();
        bundle.putString(SoapApiUtils.KEY_EMAIL,email);
        bundle.putString(SoapApiUtils.KEY_MOBILE,number);
        bundle.putString(SoapApiUtils.KEY_PASSWORD,password);
        Intent intent  = new Intent(this,VerifyEmailActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.TASKCODES.VERIFY_REGISTER);
    }

    private void setSpinners() {
        spinnerSQ1 = (Spinner) findViewById(R.id.tv_sec_qus1);
        spinnerSQ2 = (Spinner) findViewById(R.id.tv_sec_qus2);
        spinnerSQ1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                ((TextView)adapterView.getChildAt(0)).setText("Security Quesyion 1");
            }
        });
        spinnerSQ2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                ((TextView)adapterView.getChildAt(0)).setText("Security Quesyion 2");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode!=RESULT_OK){
            return;
        }
        switch (requestCode){
            case AppConstants.TASKCODES.VERIFY_REGISTER:
                showToast("Registering Customer");
                String encryptedPassword = data.getStringExtra(SoapApiUtils.KEY_ENCRYPTED_PASSWORD);
                String hashkey = data.getStringExtra(SoapApiUtils.KEY_HASHKEY);
                registerCustomer(encryptedPassword, hashkey);
        }
    }

}
