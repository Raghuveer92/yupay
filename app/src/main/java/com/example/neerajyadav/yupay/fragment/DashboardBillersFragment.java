package com.example.neerajyadav.yupay.fragment;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.model.Billers;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Neeraj Yadav on 9/21/2016.
 */

public class DashboardBillersFragment extends BaseFragment implements CustomListAdapterInterface {
    List<Billers> billersList = new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private ListView list_billers;

    public static DashboardBillersFragment getInstance() {
        return new DashboardBillersFragment();
    }

    @Override
    public void initViews() {
        list_billers = (ListView) findView(R.id.lv_dashboard_billers);
        customListAdapter = new CustomListAdapter(getActivity(),R.layout.row_dashboard_billers,billersList,this);
        list_billers.setAdapter(customListAdapter);
        setData();

    }

    private void setData() {
        for (int x=0;x<9;x++){
            Billers billers = new Billers();
            billers.setbTitle("Name"+x);
            billers.setbId("12345"+x);
            billersList.add(billers);
        }
        customListAdapter.notifyDataSetChanged();

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_dashboard_billers;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null){
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new DashboardBillersFragment.Holder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (DashboardBillersFragment.Holder) convertView.getTag();
        }
        Billers billers = billersList.get(position);
        holder.tv_title.setText(billers.getbTitle());
        holder.tv_pay_id.setText(billers.getbId());

        return convertView;
    }
    class Holder{
        TextView tv_title, tv_pay_id;

        public Holder(View view){
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_pay_id = (TextView) view.findViewById(R.id.tv_pay_id);
        }
    }
}
