package com.example.neerajyadav.yupay.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.fragment.NotificationFragment;
import com.example.neerajyadav.yupay.fragment.PasswordFragment;
import com.example.neerajyadav.yupay.fragment.ProfileFragment;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.activity.BaseActivity;

public class ProfileSettingActivity extends BaseActivity implements CustomPagerAdapter.PagerAdapterInterface {
    private ViewPager viewPager;
    List<String> tab=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);
        initToolBar("");
        initTab();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        TabLayout tableLayout = (TabLayout) findViewById(R.id.tab_layout);
        CustomPagerAdapter customPagerAdapter=new CustomPagerAdapter(getSupportFragmentManager(),tab,this);
        viewPager.setAdapter(customPagerAdapter);
        tableLayout.setupWithViewPager(viewPager);

    }

    private void initTab() {
        tab.add("Profile");
        tab.add("Notification");
        tab.add("Password");
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position){
            case 0:
                ProfileFragment profileFragment = new ProfileFragment();
                return profileFragment;
            case 1:
                NotificationFragment notificationFragment = new NotificationFragment();
                return notificationFragment;
            case 2:
                PasswordFragment passwordFragment = new PasswordFragment();
                return passwordFragment;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tab.get(position);
    }
}
