package com.example.neerajyadav.yupay.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;

import simplifii.framework.activity.BaseActivity;

public class SplashActivity extends BaseActivity {
    ImageView iv_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        iv_logo = (ImageView) findViewById(R.id.iv_logo);
        Animation animation= AnimationUtils.loadAnimation(SplashActivity.this,R.anim.fragment_slid_up);
        iv_logo.setAnimation(animation);
        animation.start();
        new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    startNextActivity(LoginActivity.class);
                    overridePendingTransition(R.anim.fragment_slid_up, R.anim.fragment_slid_down);
                    finish();
                }
            }
        }.start();
    }
}
