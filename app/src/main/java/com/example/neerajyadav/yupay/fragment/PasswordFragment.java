package com.example.neerajyadav.yupay.fragment;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.activity.LoginActivity;

import simplifii.framework.fragments.BaseFragment;

public class PasswordFragment extends BaseFragment {
    private TextInputLayout til_old_password, til_new_password, til_confirm_password;
    Button btn_submit;

    @Override
    public void initViews() {
        btn_submit= (Button) findView(R.id.btn_submit);
        til_old_password = (TextInputLayout) findView(R.id.til_old_password);
        til_new_password = (TextInputLayout) findView(R.id.til_new_password);
        til_confirm_password = (TextInputLayout) findView(R.id.til_confirm_password);
        setOnClickListener(R.id.btn_submit);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_submit){
            changePassword();
        }
        super.onClick(v);
    }

    private void changePassword() {
        String old_pass = til_old_password.getEditText().getText().toString().trim();
        String new_pass = til_new_password.getEditText().getText().toString().trim();
        String cnf_pass = til_confirm_password.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(old_pass)){
            til_old_password.setError(getString(R.string.cannot_empty));
            return;
        }else {
            til_old_password.setError(null);
        }
        if (TextUtils.isEmpty(new_pass)){
            til_new_password.setError(getString(R.string.cannot_empty));
            return;
        }else {
            til_new_password.setError(null);
        }
        if (TextUtils.isEmpty(cnf_pass)){
            til_confirm_password.setError(getString(R.string.cannot_empty));
            return;
        }else {
            til_confirm_password.setError(null);
        }

        showToast("Password Changed Successfully");
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_password;
    }
}
