package com.example.neerajyadav.yupay.activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.XmlParamObject;
import simplifii.framework.soap.requests.ValidateCustomer;
import simplifii.framework.soap.requests.ValidateCustomerRequest;
import simplifii.framework.soap.responses.ValidateCustomerResponse;
import simplifii.framework.soap.responses.ValidateCustomerResult;
import simplifii.framework.soap.responses.ValidateOTPResult;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.SoapApiUtils;
import simplifii.framework.utility.Util;

public class LoginActivity extends BaseActivity {
    private TextInputLayout til_user_id, til_password;
    private Button btn_login;
    private TextView tv_skip, tv_sign_up;
    private String emailId, pass, number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        redirectIdAlreadyLoggedIn();
        overridePendingTransition(R.anim.fragment_slid_up, R.anim.fragment_slid_down);
        til_user_id = (TextInputLayout) findViewById(R.id.til_user_id);
        til_password = (TextInputLayout) findViewById(R.id.til_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        tv_skip = (TextView) findViewById(R.id.tv_skip);
        tv_sign_up = (TextView) findViewById(R.id.tv_sign_up);

        setOnClickListener(R.id.btn_login,R.id.tv_skip,R.id.tv_sign_up );

    }

    private void redirectIdAlreadyLoggedIn() {
        ValidateCustomerResult runningInstance = ValidateCustomerResult.getRunningInstance();
        if(runningInstance!=null&&runningInstance.getVALIDATECUSTOMERRESPONSE()!=null){
            ValidateCustomerResponse savedResponse = runningInstance.getVALIDATECUSTOMERRESPONSE();
            if("0".equals(savedResponse.getRESPONSECODE())){
                startNextActivity(AccountSettingActivity.class);
                finish();
            }
        }
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_login:
                verifyLogin();
                break;
            case R.id.tv_sign_up:
                setRegister();
                break;
            case R.id.tv_skip:
                skip();
                break;
        }
    }

    private void skip() {
        startNextActivity(ForgotPasswordActivity.class);
    }

    private void setRegister() {
        startNextActivity(SignUpActivity.class);
    }

    private void verifyLogin() {
        emailId = til_user_id.getEditText().getText().toString().trim();
        pass = til_password.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(emailId)) {
            til_user_id.setError(getString(R.string.cannot_empty));
            return;
        } else if (!Util.isValidEmail(emailId)) {
            til_user_id.setError(getString(R.string.invalid_email));
            return;
        } else {
            til_user_id.setError(null);
        }
        if (TextUtils.isEmpty(pass)) {
            til_password.setError(getString(R.string.cannot_empty));
            return;
        } else {
            til_password.setError(null);
        }
        setNumber();
        Bundle bundle = new Bundle();
        bundle.putString(SoapApiUtils.KEY_EMAIL, emailId);
        bundle.putString(SoapApiUtils.KEY_PASSWORD, pass);
        bundle.putString(SoapApiUtils.KEY_MOBILE, number);
        Intent intent = new Intent(LoginActivity.this, VerifyEmailActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.TASKCODES.VERIFY_MOBILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode!=RESULT_OK){
            return;
        }
        switch (requestCode) {
            case AppConstants.TASKCODES.VERIFY_MOBILE:
                showToast("Verifying Customer");
                String encryptedPassword = data.getStringExtra(SoapApiUtils.KEY_ENCRYPTED_PASSWORD);
                validateCustomer(encryptedPassword);
                break;
        }
    }

    private void validateCustomer(String encryptedPassword) {
        ValidateCustomer validateCustomer = new ValidateCustomer();
        ValidateCustomerRequest validateCustomerRequest = new ValidateCustomerRequest();
        validateCustomerRequest.setCUSTPROFILEPASSWORD(pass);
        validateCustomerRequest.setCUSTUSERNAME(emailId);
        validateCustomer.setVALIDATECUSTOMERREQUEST(validateCustomerRequest);

        XmlParamObject xmlParamObject = new XmlParamObject();
        xmlParamObject.setActionName(SoapApiUtils.ACTION_NAME_VALIDATE_CUSTOMER);
        xmlParamObject.setClassType(ValidateCustomerResult.class);
        xmlParamObject.setJson(JsonUtil.getJsonString(validateCustomer));
        xmlParamObject.setNodeNameToParse(SoapApiUtils.NODE_NAME_VALIDATE_CUSTOMER);
        executeTask(AppConstants.TASKCODES.VALIDATE_CUSTOMER,xmlParamObject);
    }

    private void setNumber() {
        number = "";
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response==null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASKCODES.VALIDATE_CUSTOMER :
                ValidateCustomerResult validateCustomerResult = (ValidateCustomerResult) response;
                if(validateCustomerResult!=null&&validateCustomerResult.getVALIDATECUSTOMERRESPONSE()!=null){
                    if("0".equals(validateCustomerResult.getVALIDATECUSTOMERRESPONSE().getRESPONSECODE())||
                            "217".equals(validateCustomerResult.getVALIDATECUSTOMERRESPONSE().getRESPONSECODE())){
                        showToast(validateCustomerResult.getVALIDATECUSTOMERRESPONSE().getRESPONSEMESSAGE());
                        startNextActivity(AccountSettingActivity.class);
                        finish();
                    }
                    showToast(validateCustomerResult.getVALIDATECUSTOMERRESPONSE().getRESPONSEMESSAGE());
                }
        }
    }
}
