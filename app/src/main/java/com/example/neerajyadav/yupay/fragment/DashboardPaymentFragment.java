package com.example.neerajyadav.yupay.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.model.Billers;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Neeraj Yadav on 9/21/2016.
 */

public class DashboardPaymentFragment extends BaseFragment implements CustomListAdapterInterface {
    List<Billers> billersList = new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private ListView list_payment;
    @Override
    public void initViews() {
        list_payment = (ListView) findView(R.id.lv_dashboard_payment);
        customListAdapter = new CustomListAdapter(getActivity(),R.layout.row_dashboard_payment,billersList,this);
        list_payment.setAdapter(customListAdapter);
        setData();
    }

    private void setData() {
        for (int x=0;x<9;x++){
            Billers billers = new Billers();
            billers.setbTitle("Aramax"+x);
            billers.setbId("12345"+x);
            billers.setbRef("Reference No. 123456"+x);
            billers.setbDate("Jan 1"+x +"2016, 10:30PM");
            billersList.add(billers);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_dashboard_payment;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null){
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new DashboardPaymentFragment.Holder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (DashboardPaymentFragment.Holder) convertView.getTag();
        }
        Billers billers = billersList.get(position);
        holder.tv_title.setText(billers.getbTitle());
        holder.tv_pay_id.setText(billers.getbId());
        holder.tv_ref.setText(billers.getbRef());
        holder.tv_date.setText(billers.getbDate());

        return convertView;
    }

    class Holder{
        TextView tv_title, tv_pay_id, tv_ref, tv_date;

        public Holder(View view){
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_pay_id = (TextView) view.findViewById(R.id.tv_pay_id);
            tv_ref = (TextView) view.findViewById(R.id.tv_reference);
            tv_date = (TextView) view.findViewById(R.id.tv_date);

        }
    }

}
