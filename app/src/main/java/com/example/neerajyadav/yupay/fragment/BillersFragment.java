package com.example.neerajyadav.yupay.fragment;

import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.model.Billers;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

public class BillersFragment extends BaseFragment implements CustomListAdapterInterface {
    List<Billers> billersList= new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private ListView lv;

    public static BillersFragment getInstance() {
        return new BillersFragment();
    }

    @Override
    public void initViews() {
        initToolBar("");
        setHasOptionsMenu(true);
        lv = (ListView) findView(R.id.lv_billers);
        customListAdapter = new CustomListAdapter(getActivity(),R.layout.row_billers,billersList,this);
        lv.setAdapter(customListAdapter);
        setData();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add:
                BillersActionFragment billersActionFragment = new BillersActionFragment();
                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.fragment_slid_up, R.anim.fragment_slid_down);
                fragmentTransaction.add(R.id.lay_fragment_container,billersActionFragment, TAG);
                fragmentTransaction.addToBackStack(TAG);
                fragmentTransaction.commit();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected int getHomeIcon() {
        return R.drawable.bell;
    }

    private void setData() {
        for (int x=0;x<9;x++){
            Billers billers = new Billers();
            billers.setbTitle("Name"+x);
            billers.setbId("12345"+x);
            billersList.add(billers);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_billers;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if(convertView==null){
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }
        Billers billers = billersList.get(position);
        holder.tv_title.setText(billers.getbTitle());
        holder.tv_pay_id.setText(billers.getbId());

        return convertView;
    }

    class Holder{
        TextView tv_title, tv_pay_id;

        public Holder(View view){
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_pay_id = (TextView) view.findViewById(R.id.tv_pay_id);
        }
    }

}
