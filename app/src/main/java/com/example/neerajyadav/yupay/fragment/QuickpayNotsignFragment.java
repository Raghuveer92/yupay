package com.example.neerajyadav.yupay.fragment;

import com.example.neerajyadav.yupay.R;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Neeraj Yadav on 9/17/2016.
 */
public class QuickpayNotsignFragment extends BaseFragment {
    @Override
    public void initViews() {

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_quickpay_notsign;
    }
}
