package com.example.neerajyadav.yupay.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.model.Billers;
import java.util.ArrayList;
import java.util.List;
import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;

/**
 * Created by Neeraj Yadav on 9/26/2016.
 */

public class HistoryActivity extends BaseActivity implements CustomListAdapterInterface {
    List<Billers> billersList = new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private ListView lv_activity_history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        initToolBar("");
        lv_activity_history = (ListView) findViewById(R.id.lv_activity_history);
        customListAdapter = new CustomListAdapter(HistoryActivity.this,R.layout.row_activity_history,billersList,this);
        lv_activity_history.setAdapter(customListAdapter);
        setData();
    }

    private void setData() {
        for (int x=0;x<9;x++){
            Billers billers = new Billers();
            billers.setbTitle("Name"+x);
            billers.setbDate("Aug 27, 201"+x);
            billersList.add(billers);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null){
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }
        Billers billers = billersList.get(position);
        holder.tv_title.setText(billers.getbTitle());
        holder.tv_date.setText(billers.getbDate());
        return convertView;
    }

    private class Holder {
        TextView tv_title, tv_date;
        public Holder(View view){
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
        }
    }
}
