package com.example.neerajyadav.yupay.fragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.neerajyadav.yupay.R;

import simplifii.framework.fragments.BaseFragment;

public class NotificationFragment extends BaseFragment {

    @Override
    public void initViews() {

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_notification;
    }
}
