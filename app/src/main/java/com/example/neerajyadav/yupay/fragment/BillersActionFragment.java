package com.example.neerajyadav.yupay.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.activity.QuickpayReminderActivity;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Neeraj Yadav on 9/21/2016.
 */

public class BillersActionFragment extends BaseFragment {
    private LinearLayout lay_top;
    private Button btn_quickpay, btn_set_reminder, btn_edit_biller;
    @Override
    public void initViews() {
        lay_top = (LinearLayout) findView(R.id.lay_top);
        btn_quickpay = (Button) findView(R.id.btn_quickpay);
        btn_set_reminder = (Button) findView(R.id.btn_set_reminder);
        btn_edit_biller = (Button) findView(R.id.btn_edit_biller);
        setOnClickListener(R.id.lay_top,R.id.btn_quickpay,R.id.btn_set_reminder,R.id.btn_edit_biller);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lay_top:
                break;
            case R.id.btn_quickpay:
                startNextActivity(QuickpayReminderActivity.class);
                break;
        }
        super.onClick(v);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_biller_action;
    }
}
