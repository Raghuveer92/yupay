package com.example.neerajyadav.yupay.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.activity.ReminderActivity;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Neeraj Yadav on 9/22/2016.
 */

public class ReminderActionFragment extends BaseFragment {
    private LinearLayout lay_top;
    private Button btn_add_reminder, btn_add_biller;
    @Override
    public void initViews() {
        lay_top = (LinearLayout) findView(R.id.lay_top);
        btn_add_reminder = (Button) findView(R.id.btn_add_reminder);
        btn_add_biller = (Button) findView(R.id.btn_add_biller);
        setOnClickListener(R.id.btn_add_biller,R.id.btn_add_reminder,R.id.lay_top);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add_reminder:
                startNextActivity(ReminderActivity.class);
                break;
            case R.id.lay_top:
                break;
        }
        super.onClick(v);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_reminder_action;
    }
}
