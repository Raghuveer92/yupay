package com.example.neerajyadav.yupay.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.activity.ReminderActivity;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;

public class ReminderFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface {
    private ViewPager viewPager;
    List<String> tab=new ArrayList<>();

    public static ReminderFragment getInstance() {
        return new ReminderFragment();
    }


    @Override
    public void initViews() {
        initToolBar("");
        initTab();
        setHasOptionsMenu(true);
        viewPager = (ViewPager) findView(R.id.viewpager);
        TabLayout tabLayout = (TabLayout) findView(R.id.tab_layout);
        CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(getChildFragmentManager(),tab,this);
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add:
                ReminderActionFragment reminderActionFragment = new ReminderActionFragment();
                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.setCustomAnimations(R.anim.fragment_slid_up,R.anim.fragment_slid_down);
                fragmentTransaction.add(R.id.lay_fragmenu_container,reminderActionFragment).commit();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_reminder;
    }

    private void initTab() {
        tab.add("Upcoming Payment");
        tab.add("Overdue Payment");
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.bell;
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position){
            case 0:
                UpcomingPayFragment upcomingPayFragment= new UpcomingPayFragment();
                return upcomingPayFragment;
            case 1:
                OverduePayFragment overduePayFragment = new OverduePayFragment();
                return overduePayFragment;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tab.get(position);
    }
}
