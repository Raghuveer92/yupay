package com.example.neerajyadav.yupay.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.neerajyadav.yupay.R;
import com.example.neerajyadav.yupay.fragment.ScheduleReminderFragment;

import simplifii.framework.activity.BaseActivity;

/**
 * Created by Neeraj Yadav on 9/20/2016.
 */
public class QuickpayReminderActivity extends BaseActivity implements ScheduleReminderFragment.OnSelectReminder {
    private Switch btn_Switch;
    private TextView tv_schedule_date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quickpay_reminder);

        initToolBar("");

        btn_Switch = (Switch) findViewById(R.id.switch_reminder);
        tv_schedule_date= (TextView) findViewById(R.id.tv_schedule_date);

        btn_Switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    ScheduleReminderFragment scheduleReminderFragment =ScheduleReminderFragment.getInstance(QuickpayReminderActivity.this);
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.fragment_slid_up, R.anim.fragment_slid_down);
                    fragmentTransaction.add(R.id.lay_frag_container, scheduleReminderFragment);
                    fragmentTransaction.addToBackStack("");
                    fragmentTransaction.commit();
                } else {
                    tv_schedule_date.setText("");
                }

            }
        });
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.bell;
    }

    @Override
    public void onSelectDate(String time) {
        if (TextUtils.isEmpty(time)){
            btn_Switch.setChecked(false);
        }else {
            setText(time, R.id.tv_schedule_date);
        }
    }

    @Override
    public void onBackPressed() {
        if (TextUtils.isEmpty(tv_schedule_date.getText().toString())){
            btn_Switch.setChecked(false);
        }
        super.onBackPressed();
    }
}
